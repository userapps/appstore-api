const AppStoreResult = {
	RESPONSE:	'AppStoreResult@RESPONSE',
	EXCEPTION:	'AppStoreResult@EXCEPTION',
	CONNECTION:	'AppStoreResult@CONNECTION'
};

const AppStore = (function AppStore(_app) {
	const _instance			= this;
	const APP_ID			= '30558139.AppStore';
	const APP_CODE_REGED	= /(:?knuddels(AT|DE|CH|COM|TEST|DEV)\.)?([\d]+)\.([\w]+)/g;
	let _is_payed			= false;
	var _callbacks			= {};
	let _api				= [
		'mayJoinChannel',
		'mayShowPublicActionMessage',
		'mayShowPublicMessage',
		'onAccountChangedKnuddelAmount',
		'onAccountReceivedKnuddel',
		'onAppEventReceived',
		'onAppStart',
		'onBeforeKnuddelReceived',
		'onEventReceived',
		'onKnuddelReceived',
		'onPrepareShutdown',
		'onPrivateMessage',
		'onPublicActionMessage',
		'onPublicEventMessage',
		'onPublicMessage',
		'onShutdown',
		'onUserDiced',
		'onUserJoined',
		'onUserLeft',
		
		// Other API
		'onKnuddelAccountChanged'
	];
	
	function __constructor() {
		if(!isAppStoreInstalled()) {
			KnuddelsServer.getDefaultLogger().error('AppStore is **NOT** installed!');
			KnuddelsServer.getChannel().getOnlineUsers(UserType.Human).forEach(function onUser(user) {
				if(user.isChannelOwner() && user.isOnlineInChannel()) {
					user.sendPrivateMessage('Du musst die _°BB>_hAppStore|/apps install ' + APP_ID + '<°_ _°BB>installiert haben|/apps install ' + APP_ID + '<r°_, damit du diese UserApp nutzen kannst.');
				}
			});
		}
		
		/* Create cloned API */
		_api.forEach(function onAppMethod(_method) {
			if(typeof(_app[_method]) != 'undefined') {
				_instance[_method] = function methodHook() {
					createMethodHook(_method, arguments);
				};
			} else if(_method == 'onAppEventReceived') {
				_instance[_method] = function methodHook() {
					createMethodHook(_method, arguments);
				};
			}
		}.bind(this));
		
		if(typeof(_app.chatCommands) != 'undefined') {
			this.chatCommands = {};
			
			Object.keys(_app.chatCommands).forEach(function onChatCommand(_command) {
				this.chatCommands[_command] = function commandHook(user, parameters, command) {
					createCommandHook(_command, user, parameters, command);
				};
			}.bind(this));
		}
	}
	
	function getAppInfo() {
		return getAppCode(KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getAppId());
	};
	
	function createMethodHook(_method, args) {
		var user = null;
		
		switch(_method) {
			case 'onAppStart':
				AppStoreRequest('payment_check', function onSuccess(type, data) {
					if(typeof(data.info) != 'undefined' && data.info == 'ALREADY_PAYED') {
						_is_payed = true;
						_app[_method].apply(_app, args);
					}
				//	KnuddelsServer.getDefaultBotUser().sendPublicMessage(JSON.stringify(data));
				}, function onError(type, data) {});
			break;
			case 'onUserJoined':
				user = args[0];
			break;
			case 'onAccountReceivedKnuddel':
				user = args[0];
			break;
			case 'onBeforeKnuddelReceived':
				user = args[0];
			break;
			case 'onAppEventReceived':
				if(checkAppCode(args[0].getAppInfo().getAppId(), APP_ID)) {
					switch(args[1]) {
						case 'payment_check':
							updatePaymentCheck(args[1], args[2]);
						break;
					}
					return;
				}
			break;
			case 'onEventReceived':
				user = args[0];
			break;
			case 'onKnuddelReceived':
				user = args[0];
			break;
		}
		
		if(!isPayed()) {
			if(user != null && user.isChannelOwner()) {
				AppStoreRequest('payment_check', function onSuccess(type, data) {}, function onError(type, data) {
					user.sendPrivateMessage('Du musst die UserApp _' + getAppInfo() + '_ erst _°BB>bezahlen|/AppStore buy:' + getAppInfo() + '<r°_, bevor du diese verwenden kannst.');
				});
				return;
			}
			
			if(user != null) {
				user.sendPrivateMessage('Die UserApp ist derzeit _deaktiviert_!');
			}
			return;
		}
		
		_app[_method].apply(_app, args);
	};
	
	function createCommandHook(_command, user, parameters, command) {
		if(!isPayed()) {
			if(user.isChannelOwner()) {
				AppStoreRequest('payment_check', function onSuccess(type, data) {}, function onError(type, data) {
					user.sendPrivateMessage('Du musst die UserApp _' + getAppInfo() + '_ erst _°BB>bezahlen|/AppStore buy:' + getAppInfo() + '<r°_, bevor du diese verwenden kannst.');
				});
				return;
			}
			
			user.sendPrivateMessage('Die UserApp ist derzeit _deaktiviert_!');
			return;
		}
		
		_app.chatCommands[_command].call(_app, user, parameters, command);
	};
	
	function isAppStoreInstalled() {
		var found = false;
		
		KnuddelsServer.getAppAccess().getAllRunningAppsInChannel().forEach(function onAppInstance(instance) {
			var info		= instance.getAppInfo();
			var developer	= info.getAppDeveloper();
			var code		= info.getAppKey();
			var version		= info.getAppVersion();
			var id			= info.getAppId();
			
			if(checkAppCode(id, APP_ID)) {
				found = true;
			}
		});
		
		return found;
	};
	
	function getAppStore() {
		var store = null;
		
		KnuddelsServer.getAppAccess().getAllRunningAppsInChannel().forEach(function onAppInstance(instance) {
			if(checkAppCode(instance.getAppInfo().getAppId(), APP_ID)) {
				store = instance;
				return;
			}
		});
		
		return store;
	};
	
	function checkAppCode(code, equals) {
		var found = false;
		var matches;

		while((matches = APP_CODE_REGED.exec(code)) !== null) {
			if(matches.index === APP_CODE_REGED.lastIndex) {
				APP_CODE_REGED.lastIndex++;
			}
			
			if((matches[3] + '.' + matches[4]) == equals) {
				found = true;	
			}
		};
		
		return found;
	};
	
	function getAppCode(code) {
		var result = null;
		var matches;

		while((matches = APP_CODE_REGED.exec(code)) !== null) {
			if(matches.index === APP_CODE_REGED.lastIndex) {
				APP_CODE_REGED.lastIndex++;
			}
			
			if(result == null) {
				result = matches[3] + '.' + matches[4];
			}
		};
		
		matches = null;
		delete matches;
		
		return result;
	};
	
	function AppStoreRequest(action, callback_success, callback_error) {
		var _watcher = setInterval(function() {
			if(typeof(App) != 'undefined') {
				clearInterval(_watcher);
				var owner	= [];
				var store	= getAppStore();
		
				if(store == null) {
					callback_error(AppStoreResult.CONNECTION, null);
					return;
				}
				
				KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners().forEach(function(user) {
					owner.push(user.getUserId());
				});
		
				_callbacks[action] = {
					success:	callback_success,
					error:		callback_error
				};
				
				store.sendAppEvent(action, {
					app_id:	getAppInfo(),
					owner:	owner.join(',')
				});
				
				owner = [];
				delete owner;
				return;
			}
		}, 100);
	};
		
	function updatePaymentCheck(action, data) {
		_is_payed = data.state;
		
		if(!isPayed()) {
			_callbacks[action].error(AppStoreResult.RESPONSE, data);
			/*
			KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners().forEach(function(user) {
				if(user.isOnlineInChannel()) {
					user.sendPrivateMessage('Du musst die UserApp _' + getAppInfo() + '_ erst _°BB>bezahlen|/AppStore buy:' + getAppInfo() + '<r°_, bevor du diese verwenden kannst (REASON ' + data.info.escapeKCode() + ').');
				}
			});*/
		} else {
			_callbacks[action].success(AppStoreResult.RESPONSE, data);
		}
		
		_callbacks[action] = null;
		delete _callbacks[action];
	}
	
	function isPayed() {
		return _is_payed;
	};
	
	__constructor.call(this);
});