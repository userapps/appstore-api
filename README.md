AppStore API
===================

Die `AppStore API` wird f�r eine UserApp verwendet, sofern diese **kostenpflichtig** ist.

### �bersicht
---

[TOC]

### API
---
> In der Regel braucht du **nicht pr�fen**, ob die AppStore bereits installiert ist oder deine UserApp bezahlt wurde; Dies geschieht durch die API voll automatisch!

```
App.isPayed() // Pr�ft, ob die App bereits bezahlt ist
App.isAppStoreInstalled() // Pr�ft, ob die AppStore installiert ist
```

### Installation
---
>- [ ] Lade die `AppStore.API.js` herunter
- [ ] Lade die `AppStore.API.js` �ber `FTP` in deinem App-Ordner hoch
- [ ] Binde die `AppStore.API.js` in deiner UserApp mit `require` ein
- [ ] Wrappe deinen `AppContainer` mit der `AppStore`

#### **Beispiel** (API einbinden)
```
require('AppStore.API.js');
```
#### **Beispiel** (AppStore-Wrapper)
Dein AppContainer sieht folgenderma�en aus:
```
var App = (new function AppContainer() {
	this.onAppStart() {
		// Deine App wird gestartet
	};
});
```
F�ge hier den AppStore-Wrapper hinzu: 
```
var App = new AppStore(/* Code deiner UserApp */);
```
**Ergebnis:**
```
var App = new AppStore(new function AppContainer() {
	this.onAppStart() {
		// Deine App wird gestartet
	};
});
```
> Du brauchst lediglich ein `new AppStore` vor deinen AppContainer setzen!

Deine App ist nun einsatzbereit!

### Funktionsweise
---
Der `AppStore`-Wrapper sch�tzt alle deine UserApp-Funktionen. Wenn du Beispielsweise `onAppStart` oder `onUserJoined` in deiner UserApp integriert hast, wird bei Aufruf vorher gepr�ft, ob:

- Die AppStore im Channel installiert ist
- Deine UserApp bereits bezahlt wurde

Das selbe gillt auch f�r `ChatCommands`; Werden diese von einem normalen Nutzer aufgerufen, erscheint eine Meldung, dass die Funktion derzeit nicht zur Verf�gung steht. Wenn der Channel-Besitzer diese aufruft, wird dieser zur Zahlung aufgefordert, sofern dies noch nicht geschehen ist.

> Den Preis deiner UserApp legst du in unserem Backend fest!

### UserApp-Installation ohne AppStore
---
Was ist, wenn der Nutzer die User-App ID erlangt und diese Beispielsweise durch `/apps install 123456.DeineApp` selbst installiert?

Dies kann er ohne Probleme tun - Ist die AppStore nicht installiert oder die App nicht bezahlt, wird diese nicht freigeschaltet.

### UserApp Konfiguration
---
Sofern du UserApps in die AppStore einstellen m�chtest, **musst** du die Installation f�r jeden Nutzer zulassen. Dies kannst du in der `app.config` deiner UserApp mit folgender Zeile setzen:
> mayBeInstalledBy.1 = *

UserApps, die nicht f�r jeden freigegeben werden, werden zur Ver�ffentlichung abgelehnt.